# Vue.js widget demo with Koa api

## Build and run backend

Edit docker-compose.yaml file and specify MONGO_URI if you want to use  
existing database.

Then build and launch:  
`docker-compose build && docker-compose up -d`

## Build widget

To build widget, use this command:  
`export API_URL=http://your-api-host`  
`npm i && npm run build && npm run pack`

Will appear folder `build` with two files inside: widget.js and widget.css.  
Then add following code to HTML head of your page:  

    <link href="./widget.css" rel="stylesheet"></head>


And this part before body closure tag:  

    <div id="donate-widget"></div>
    <script src="./widget.js"></script>

That's it!
