#!/bin/sh

# compress and rename, join Js files into one
npx webpack --entry ./dist/js/*.js --output-path ./build
mv ./dist/css/*.css ./build/widget.css
cd ./build; mv main.js widget.js;
echo done
