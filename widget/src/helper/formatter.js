
export const formatCurrency = (number) => {
	return new Intl.NumberFormat('en-US').format(number)
}

export const prettyNumber = (number, code) => {
	if (code === 'RUB') {
		if (number > 100000) {
			return (number - number%50000);
		}

		if (number > 30000) {
			return (number - number%10000);
		}

		if (number > 10000) {
			return (number - number%5000);
		}

		if (number > 1000) {
			return (number - number%2000);
		}

		return number - number%500;
	}

	if (number > 1000) {
		return (number - number%500);
	}

	if (number > 100) {
		return (number - number%100);
	}

	return number - number%20;
}
