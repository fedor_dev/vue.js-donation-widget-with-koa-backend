import axios from 'axios'

const API_HOST = process.env.API_HOST || 'http://localhost:3700';

export const submitDonation = async (amount, currency) => {
	if (!amount || amount < 1 || !currency) {
		throw new Error('Incorrect value');
	}

	const req = await axios.post(API_HOST + '/donate', {
		amount,
		currency,
	}).catch(() => false);

	if (!req || req.status != 200) {
		throw new Error('Failed to send request');
	}

	return true;
}
