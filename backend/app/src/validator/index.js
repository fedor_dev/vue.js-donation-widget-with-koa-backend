
const currencies = ['USD', 'EUR', 'GBP', 'RUB'];

const validate = (data) => {
	if (!data.amount) {
		throw new Error('Amount required');
	}

	if (data.amount < 1) {
		throw new Error('Incorrect amount')
	}

	if (!data.currency) {
		throw new Error('Currency required')
	}

	if (!currencies.includes(data.currency)) {
		throw new Error('Invalid currency')
	}

	return true;
}

module.exports = validate;
