const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let donation = new Schema(
  {
    currency: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
  },
  {
    collection: "Donations",
    timestamps: true,
  }
);

module.exports = mongoose.model("donations", donation);
