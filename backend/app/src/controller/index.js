const mongoose = require('mongoose')
const Donation = require('../schema/donation')

const getList = function() {
	return Donation.find();
}

const addDonation = function(data) {
	return Donation.create(data);
}

module.exports = {
	getList,
	addDonation,
};
