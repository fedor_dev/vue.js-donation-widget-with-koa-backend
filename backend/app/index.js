const Koa = require("koa");
const mongoose = require('mongoose')
const router = require("@koa/router")();
const cors = require('@koa/cors');
const bodyParser = require("koa-bodyparser");

const app = new Koa();
const validator = require('./src/validator/')
const controller = require('./src/controller/');

app.use(bodyParser());
app.use(cors()); // allow requests from any origin

const dbUri = process.env.MONGO_URI || "mongodb://localhost:27017/demo";
mongoose.connect(dbUri, {
  useUnifiedTopology: true,
  useNewUrlParser: true
});

router.post("/donate", async (ctx, next) => {
  const data = ctx.request.body
  try {
    await validator(data);
    await controller.addDonation(data);
    ctx.status = 200;
    ctx.body = { ok: true }
  } catch (err) {
    ctx.status = 400;
    ctx.body = { ok: false, error: err.message }
    next();
  }
});

router.get("/list", async (ctx, next) => {
  ctx.status = 200;
  const data = await controller.getList();
  ctx.body = data;
});

app.use(router.routes());
app.listen(3700);
